import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/post/:id",
    name: "PostRead",
    component: () => import("@/views/PostRead"),
  },
  {
    path: "/create",
    name: "CreatePost",
    component: () => import("@/views/CreatePost"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
